import matplotlib.pyplot as plt

import common


def plot_dimension_combinations(X, y, subsample, labels, file_prefix, random_state):
    filenames = []
    dimensions = X.shape[1]
    (X, y) = common.reduce_by_subsample(X, y, subsample, random_state)

    for d1 in range(0, dimensions):
        for d2 in range(d1 + 1, dimensions):
            X1 = X[:, d1]
            X2 = X[:, d2]

            plt.figure()
            plt.xlabel(labels[d1])
            plt.ylabel(labels[d2])
            cdict = {-1: 'blue', 1: 'red'}
            colors = list(map((lambda y: cdict.get(y)), y))
            plt.scatter(X1, X2, c=colors, s=1)
            filename = 'milestone3/images/' + file_prefix + 'd%d - d%d.png' % (d1, d2)
            filenames.append(filename)
            plt.savefig(filename)
            plt.close()

    return filenames
