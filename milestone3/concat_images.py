import numpy as np
import matplotlib.pyplot as plt
from functools import reduce


def concat_images(image_paths, num_columns):
    split = np.array_split(image_paths, num_columns)
    strips = (reduce(concat_horizontal, image) for image in split)
    return reduce(concat_vertical, strips)


def concat_horizontal(imga, imgb):
    """
    Combines two color image ndarrays side-by-side.
    """
    if isinstance(imga, str):
        imga = plt.imread(imga, format='png')
    if isinstance(imgb, str):
        imgb = plt.imread(imgb, format='png')

    ha,wa = imga.shape[:2]
    hb,wb = imgb.shape[:2]
    max_height = np.max([ha, hb])
    total_width = wa+wb
    new_img = np.zeros([max_height, total_width, 4])
    new_img[:ha, :wa] = imga
    new_img[:hb, wa:wa+wb] = imgb
    return new_img


def concat_vertical(imga, imgb):
    """
    Combines two color image ndarrays top and bottom.
    """
    if isinstance(imga, str):
        imga = plt.imread(imga)
    if isinstance(imgb, str):
        imgb = plt.imread(imgb)

    ha,wa = imga.shape[:2]
    hb,wb = imgb.shape[:2]
    max_width = np.max([wa, wb])
    total_height = ha+hb
    new_img = np.zeros([total_height, max_width, 4])
    # new_img = np.zeros(shape=(total_height, max_width, 4))
    new_img[:ha, :wa] = imga
    new_img[ha:ha+hb, :wb] = imgb
    return new_img

