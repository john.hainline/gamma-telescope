import numpy as np
from sklearn.decomposition import (PCA)
from sklearn.linear_model import LogisticRegression
from sklearn.gaussian_process.kernels import (RBF)
from sklearn.neural_network import MLPClassifier
import matplotlib.pyplot as plt
from common.gp_roc_curve import gp_roc_curve

import common


def plot_pca_comparisons(X, y, subsample_size, folds, random_state):
    lin_classifier = LogisticRegression()
    nn_classifier = MLPClassifier(solver='lbfgs', alpha=1e-4, hidden_layer_sizes=(7))

    fp_domain = np.linspace(0, 1, 100)
    lin_plots = []
    gp_plots = []
    nn_plots = []
    legends = []
    for n in range(1, 11):
        pca = PCA(n_components=n)
        X_transformed = pca.fit_transform(X)

        lin_curve = common.generate_roc_curve(lin_classifier, X_transformed, y, folds=folds)
        lin_plots.append(lin_curve.tp_means)

        gp_curve = gp_roc_curve(common.reduce_by_subsample, RBF(), X_transformed, y, subsample_size, folds, random_state)
        gp_plots.append(gp_curve.tp_means)

        nn_curve = common.generate_roc_curve(nn_classifier, X_transformed, y, folds=folds)
        nn_plots.append(nn_curve.tp_means)

        legends.append("n = %d" % n)

        print('--ROC Curve--')
        print("pca: n = %d" % n)
        print('Linear AUC:\t%f' % lin_curve.auc_mean)
        print('GP AUC:\t\t%f' % gp_curve.auc_mean)
        print('NN AUC:\t\t%f\n' % nn_curve.auc_mean)

    for title, plots, file in [
            ('ROC Curve with Linear Classifier after PCA Reduction to (n)', lin_plots, 'linear_pca'),
            ('ROC Curve with GP Classifier after PCA Reduction to (n)', gp_plots, 'gp_pca'),
            ('ROC Curve with NN Classifier after PCA Reduction to (n)', nn_plots, 'nn_pca')]:
        plt.figure()
        plt.xlabel('False Positive Rate')
        plt.ylabel('True Positive Rate')
        plt.title(title)
        for plot in plots:
            plt.plot(fp_domain, plot)
        plt.legend(legends, loc='lower right')
        plt.savefig('milestone3/images/' + file + '.png')
        plt.close()
