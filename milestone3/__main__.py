from sklearn.preprocessing import RobustScaler
from sklearn.decomposition import (PCA)
import matplotlib.pyplot as plt
from milestone3.plot_pca_comparisons import plot_pca_comparisons
from milestone3.plot_dimension_combinations import plot_dimension_combinations
from milestone3.concat_images import concat_images
import common

RANDOM_SEED = 1234
K_FOLDS = 10
SUBSAMPLE_SIZE = 1500

data = common.load_data()
X, y = common.split_xy(data)
X_scaled = RobustScaler().fit_transform(X)

dimensions = X.shape[1]
pca = PCA(n_components=dimensions)
X_transformed = pca.fit_transform(X_scaled)
labels = []
for dim in range(1, dimensions + 1):
    labels.append('d%d' % dim)

print('PCA Singular Values [X]:')
print(PCA(n_components=dimensions).fit(X).singular_values_)
print('PCA Singular Values [X_scaled]:')
print(PCA(n_components=dimensions).fit(X_scaled).singular_values_)
print('\n')

plot_pca_comparisons(X_scaled, y, SUBSAMPLE_SIZE, K_FOLDS, RANDOM_SEED)

standard_images = plot_dimension_combinations(X_scaled, y, SUBSAMPLE_SIZE, common.names, 'standard ', RANDOM_SEED)
standard_image = concat_images(standard_images, 9)
plt.imsave(arr=standard_image, fname='milestone3/images/standard.png', format='png')

pca_images = plot_dimension_combinations(X_transformed, y, SUBSAMPLE_SIZE, labels, 'pca ', RANDOM_SEED)
pca_image = concat_images(pca_images, 9)
plt.imsave(arr=pca_image, fname='milestone3/images/pca.png', format='png')
