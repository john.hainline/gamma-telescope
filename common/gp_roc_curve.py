
import numpy as np
import collections

from sklearn.gaussian_process import (GaussianProcessClassifier)
from sklearn.metrics import roc_curve, roc_auc_score
from sklearn.model_selection import (StratifiedKFold)

Curve = collections.namedtuple('Curve', ['fp_domain', 'tp_means', 'auc_mean'])


def gp_roc_curve(REDUCER, kernel, X, y, size, folds, random_state):
    cv = StratifiedKFold(n_splits=folds, shuffle=True, random_state=random_state)

    fp_domain = np.linspace(0, 1, 100)
    classifier = GaussianProcessClassifier(kernel=kernel)
    auc_list = []
    tp_list = []

    for train, test in cv.split(X, y):
        X_train, y_train = REDUCER(X[train], y[train], size, random_state)
        classifier.fit(X_train, y_train)

        prob = classifier.predict_proba(X[test])[:, 1]

        fp_by_thresh, tp_by_thresh, thresh = roc_curve(y[test], prob)
        auc = roc_auc_score(y[test], prob)

        tp_by_fp = np.interp(fp_domain, fp_by_thresh, tp_by_thresh)

        tp_list.append(tp_by_fp)
        auc_list.append(auc)

    tp_means = np.mean(tp_list, axis=0)
    auc_mean = np.mean(auc_list)
    return Curve(fp_domain, tp_means, auc_mean)
