from sklearn.model_selection import StratifiedKFold, cross_val_score


def calculate_cv_scores(classifier, X, y, folds=10, random_state=0):
    cv = StratifiedKFold(n_splits=folds, shuffle=True, random_state=random_state)
    return cross_val_score(classifier, X, y, cv=cv)
