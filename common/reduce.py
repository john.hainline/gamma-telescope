import numpy as np
from sklearn.cluster import (MiniBatchKMeans)
from sklearn.model_selection import StratifiedShuffleSplit


def reduce_by_kmeans(X, y, k, random_state):
    kmeans = MiniBatchKMeans(n_clusters=k, batch_size=1000, random_state=random_state)
    cluster_idx = kmeans.fit_predict(X)
    votes = np.array([np.sign(np.sum(y[cluster_idx == idx])) for idx in range(k)])
    votes[votes == 0] = -1  # HACK: bias towards negative
    return kmeans.cluster_centers_, votes


def reduce_by_subsample(X, y, k, random_state):
    shuffle_split = StratifiedShuffleSplit(n_splits=1, test_size=k, random_state=random_state)
    _, idx = next(shuffle_split.split(X, y))
    return X[idx], y[idx]
