import numpy as np
import collections

from sklearn.model_selection import StratifiedKFold
from sklearn.metrics import roc_curve, roc_auc_score

Curve = collections.namedtuple('Curve', ['fp_domain', 'tp_means', 'auc_mean'])


def generate_roc_curve(classifier, X, y, folds=10, random_state=0):
    cv = StratifiedKFold(n_splits=folds, shuffle=True, random_state=random_state)

    auc_list = []
    tp_list = []
    fp_domain = np.linspace(0, 1, 100)

    for train, test in cv.split(X, y):
        classifier.fit(X[train], y[train])

        prob = classifier.predict_proba(X[test])[:, 1]

        fp_by_thresh, tp_by_thresh, thresh = roc_curve(y[test], prob)
        auc = roc_auc_score(y[test], prob)

        tp_by_fp = np.interp(fp_domain, fp_by_thresh, tp_by_thresh)

        tp_list.append(tp_by_fp)
        auc_list.append(auc)

    tp_means = np.mean(tp_list, axis=0)
    auc_mean = np.mean(auc_list)

    return Curve(fp_domain, tp_means, auc_mean)
