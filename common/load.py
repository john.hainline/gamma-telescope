import numpy as np
import pandas as pd

# Maps classes to binary label
LABELS = {
    'g': 1,
    'h': -1
}

# List of (name, type) tuples for columns of data set
COLUMNS = [
    ('fLength',  np.double),
    ('fWidth',   np.double),
    ('fSize',    np.double),
    ('fConc',    np.double),
    ('fConc1',   np.double),
    ('fAsym',    np.double),
    ('fM3Long',  np.double),
    ('fM3Trans', np.double),
    ('fAlpha',   np.double),
    ('fDist',    np.double),
    ('class',    np.object),
]

names, _ = zip(*COLUMNS)
dtypes = dict(COLUMNS)


def load_data():
    # Load in dataset, return as (X, y) tuple, where X are features and y are labels
    data = pd.read_csv('dataset/magic04.data', names=names, dtype=dtypes)
    data.replace({'class': LABELS}, inplace=True)
    return data


def split_xy(data):
    # if isinstance(data,(list,pd.core.series.Series,np.ndarray)):
    if isinstance(data, pd.DataFrame):
        X = data.drop('class', axis=1).as_matrix()
        y = data['class'].as_matrix()
    else:
        X = data[:, 0:-1]
        y = data[:, -1]
    return X, y
