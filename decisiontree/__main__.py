import numpy as np
from sklearn.preprocessing import RobustScaler
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier
import matplotlib.pyplot as plt

import common
import time

data = common.load_data()
X, y = common.split_xy(data)
X_scaled = RobustScaler().fit_transform(X)


### EXPERIMENT ONE: varying number of trees ###

TREE_RANGE = [4, 8, 16, 32, 64, 128, 256, 512]
MAX_DEPTH = None
N_JOBS = -1
K_FOLDS = 10

roc_curves = [None for i in TREE_RANGE]
fp_domain = None
mean_tp_curves = []

mean_aucs = []

for n_trees in TREE_RANGE:
    print("Running n_trees=%d..." % n_trees)
    classifier = RandomForestClassifier(
        n_estimators = n_trees,
        max_depth = MAX_DEPTH,
        n_jobs=N_JOBS)

    curve = common.generate_roc_curve(classifier, X_scaled, y, folds=K_FOLDS)
    fp_domain = curve.fp_domain
    mean_tp_curves.append(curve.tp_means)
    mean_aucs.append(curve.auc_mean)
    print("AUC: %f" % curve.auc_mean)

# ROC Curves by Number of Trees
plt.figure()
plt.xlabel('False Positive Rate')
plt.ylabel('True Positive Rate')
plt.title('ROC Curve by Forest Size (%d-fold cross validation)' % K_FOLDS)
for tp_mean in mean_tp_curves:
    plt.plot(fp_domain, tp_mean)
plt.legend(TREE_RANGE, loc='lower right')
plt.savefig('decisiontree/roc_by_ntrees.png')
# plt.show()


# AUC by Number of Trees

plt.figure()
plt.xlabel('Number of Trees in Forest')
plt.ylabel('Area Under ROC Curve')
plt.title('AUC by Forest Size (%d-fold cross validation)' % K_FOLDS)
plt.plot(TREE_RANGE, mean_aucs)
plt.savefig('decisiontree/auc_by_ntrees.png')
# plt.show()


### EXPERIMENT TWO: Depth Restriction ###


N_TREES = 128
MAX_DEPTH_RANGE = [1, 2, 4, 8, 16, 32, 64, 128, 256, 512]
N_JOBS = -1
K_FOLDS = 10

roc_curves = [None for i in MAX_DEPTH_RANGE]
fp_domain = None
mean_tp_curves = []
mean_aucs = []

for max_depth in MAX_DEPTH_RANGE:
    print("Running max_depth=%d..." % max_depth)
    classifier = RandomForestClassifier(
        n_estimators = N_TREES,
        max_depth = max_depth,
        n_jobs=N_JOBS)

    curve = common.generate_roc_curve(classifier, X_scaled, y, folds=K_FOLDS)
    fp_domain = curve.fp_domain
    mean_tp_curves.append(curve.tp_means)
    mean_aucs.append(curve.auc_mean)
    print("AUC: %f" % curve.auc_mean)

# ROC Curves by Number of Trees
plt.figure()
plt.xlabel('False Positive Rate')
plt.ylabel('True Positive Rate')
plt.title('ROC Curve by Tree Depth (%d-fold cross validation)' % K_FOLDS)
for tp_mean in mean_tp_curves:
    plt.plot(fp_domain, tp_mean)
plt.legend(MAX_DEPTH_RANGE, loc='lower right')
plt.savefig('decisiontree/roc_by_depth.png')
# plt.show()


# AUC by Number of Trees

plt.figure()
plt.xlabel('Max Depth for Trees')
plt.ylabel('Area Under ROC Curve')
plt.title('AUC by Tree Depth (%d-fold cross validation)' % K_FOLDS)
plt.plot(MAX_DEPTH_RANGE, mean_aucs)
plt.savefig('decisiontree/auc_by_depth.png')
# plt.show()
