import random
import time
from itertools import permutations
from operator import itemgetter

import numpy as np
from sklearn.metrics import roc_curve, roc_auc_score
from sklearn.model_selection import StratifiedKFold

from sklearn.preprocessing import RobustScaler
from sklearn.linear_model import LogisticRegression
from sklearn.neural_network import MLPClassifier
from sklearn.gaussian_process.kernels import RBF
from sklearn.gaussian_process import GaussianProcessClassifier
from sklearn.ensemble import RandomForestClassifier

import matplotlib.pyplot as plt

from scipy.stats import mannwhitneyu

import common
from milestone2.reducing_classifier import ReducingClassifier

SIGNIFICANCE = 0.05

RUNS = 10
FOLDS = 10

CLASSIFIERS = {
    'Logistic Regression' : LogisticRegression(),
    'GPC' : ReducingClassifier(GaussianProcessClassifier(kernel=RBF()), common.reduce_by_subsample, 1500),
    'Neural Net' : MLPClassifier(solver='lbfgs', alpha=2e-3, hidden_layer_sizes=(16,16,8), activation='tanh'),
    'Random Forest' : RandomForestClassifier(n_estimators=128, max_depth=None, n_jobs=-1)
}

data = common.load_data()
X, y = common.split_xy(data)
X_scaled = RobustScaler().fit_transform(X)

roc_domain = np.linspace(0, 1, 100)
aucs = { key:list() for key in CLASSIFIERS }
rocs = { key:list() for key in CLASSIFIERS }

train_times = { key:0 for key in CLASSIFIERS }
test_times = { key:0 for key in CLASSIFIERS }

for run in range(RUNS):
    print('Run %d...' % (run+1))
    cv = StratifiedKFold(n_splits=FOLDS, shuffle=True)

    fold = 1
    for train, test in cv.split(X_scaled, y):
        print('Fold %d' % fold)
        fold += 1

        for name, classifier in CLASSIFIERS.items():

            train_start = time.time()
            classifier.fit(X_scaled[train], y[train])
            train_time = time.time() - train_start

            test_start = time.time()
            prob = classifier.predict_proba(X_scaled[test])[:, 1]
            test_time = time.time() - test_start

            fp_by_thresh, tp_by_thresh, thresh = roc_curve(y[test], prob)
            roc = np.interp(roc_domain, fp_by_thresh, tp_by_thresh)

            auc = roc_auc_score(y[test], prob)

            train_times[name] += train_time
            test_times[name] += test_time
            rocs[name].append(roc)
            aucs[name].append(auc)

mean_rocs = {k: np.mean(v, axis=0) for (k, v) in rocs.items()}
mean_aucs = {k: np.mean(v) for (k, v) in aucs.items()}

plt.figure()
plt.xlabel('False Positive Rate')
plt.ylabel('True Positive Rate')
plt.title('ROC Curves')
legend = []
for key, roc in mean_rocs.items():
    legend.append(key)
    plt.plot(roc_domain, roc)
plt.legend(legend, loc='lower right')
plt.savefig('milestone4/roc_curves.png')
plt.show()

print("----Mean AUCs----")
for pair in mean_aucs.items():
    print("%s: %f" % pair)

print("\n----Total Train Times----")
for pair in train_times.items():
    print("%s: %f" % pair)

print("\n----Total Test Times----")
for pair in test_times.items():
    print("%s: %f" % pair)

print("\n----Mann-Whitney U Test----")

results = []
for (name_a, aucs_a), (name_b, aucs_b) in permutations(aucs.items(), 2):
    p = mannwhitneyu(aucs_a, aucs_b, alternative='greater').pvalue
    print('%s > %s : p-value %f' % (name_a, name_b, p))
    results.append((name_a,name_b,p))

print("\n----Holm-Bonferonni Correction----")

results.sort(key=itemgetter(2))

n = len(results)
for (rank, (name_a, name_b, p)) in enumerate(results):
    threshold = SIGNIFICANCE / (n - rank)

    if p < threshold:
        print('%s > %s:\t%f < %f\tSUCCESS' % (name_a, name_b, p, threshold))
    else:
        print('%s > %s:\t%f >= %f\tFAILURE' % (name_a, name_b, p, threshold))
