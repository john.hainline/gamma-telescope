import numpy as np
import matplotlib.pyplot as plt
import common
from common.gp_roc_curve import gp_roc_curve
from sklearn.preprocessing import RobustScaler
from sklearn.gaussian_process.kernels import (RBF, Matern, WhiteKernel)

RANDOM_SEED = 1234
K_FOLDS = 10

X, y = common.split_xy(common.load_data())
X_scaled = RobustScaler().fit_transform(X)

size_range = range(100, 1100, 100)
fp_domain = np.linspace(0, 1, 100)
plots_subsample = []
plots_kmeans = []
plots_final = []
size_legends = []

for size in size_range:
    kernel = RBF()
    curve_subsample = gp_roc_curve(common.reduce_by_subsample, kernel, X_scaled, y, size, K_FOLDS, RANDOM_SEED)
    curve_kmeans = gp_roc_curve(common.reduce_by_kmeans, kernel, X_scaled, y, size, K_FOLDS, RANDOM_SEED)
    plots_subsample.append(curve_subsample.tp_means)
    plots_kmeans.append(curve_kmeans.tp_means)
    size_legends.append('n=%d' % size)
    print('Size:\t%d\tMethod:\tSubsample\tAUC:\t%f' % (size, curve_subsample.auc_mean))
    print('Size:\t%d\tMethod:\tKMeans\t\tAUC:\t%f' % (size, curve_kmeans.auc_mean))

plt.figure()
plt.xlabel('False Positive Rate')
plt.ylabel('True Positive Rate')
plt.title('ROC Curve (%d-fold cross validation)' % K_FOLDS)
for plot in plots_subsample:
    plt.plot(fp_domain, plot)
plt.legend(size_legends, loc='lower right')
plt.savefig('milestone2/subsample_roc_curve.png')
plt.show()

plt.figure()
plt.xlabel('False Positive Rate')
plt.ylabel('True Positive Rate')
plt.title('ROC Curve (%d-fold cross validation)' % K_FOLDS)
for plot in plots_kmeans:
    plt.plot(fp_domain, plot)
plt.legend(size_legends, loc='lower right')
plt.savefig('milestone2/kmeans_roc_curve.png')
plt.show()

size_for_kernels = 200
kernel_legends = []
for kernel, legend in [(RBF(), 'RBF'), (RBF(length_scale=10000.0), 'RBF-Long'), (Matern(nu=1.5) + WhiteKernel(), 'Matern+White')]:
    curve = gp_roc_curve(common.reduce_by_subsample, kernel, X_scaled, y, size_for_kernels, K_FOLDS, RANDOM_SEED)
    plots_final.append(curve.tp_means)
    kernel_legends.append(legend)
    print('Size:\t%d\tMethod:\t%s\t\tAUC:\t%f' % (size_for_kernels, legend, curve.auc_mean))

plt.figure()
plt.xlabel('False Positive Rate')
plt.ylabel('True Positive Rate')
plt.title('ROC Curve (%d-fold cross validation)' % K_FOLDS)
for plot in plots_final:
    plt.plot(fp_domain, plot)
plt.legend(kernel_legends, loc='lower right')
plt.savefig('milestone2/kernels_roc_curve.png')
plt.show()
