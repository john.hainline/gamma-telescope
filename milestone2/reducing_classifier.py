class ReducingClassifier:
    """Wrapper for classifiers to reduce dataset before training"""

    def __init__(self, classifier, reducer, size, random_state=None):
        self.classifier = classifier
        self.reducer = reducer
        self.size = size
        self.random_state = random_state

    def fit(self,x,y):
        X_train, y_train = self.reducer(x, y, self.size, self.random_state)
        self.classifier.fit(X_train, y_train)

    def predict(self, x):
        return self.classifier.predict(x)

    def predict_proba(self, x):
        return self.classifier.predict_proba(x)
