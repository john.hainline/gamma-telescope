import pandas as pd
from sklearn.linear_model import LogisticRegression
import matplotlib.pyplot as plt

import common

data = common.load_data()
X, y = common.split_xy(data)
classifier = LogisticRegression()
K_FOLDS = 10

# Describe Data
print('--Data Description--')
pd.set_option('display.max_columns', None)
pd.set_option('display.max_rows', None)
desc = data.describe(include='all').round(6)
desc.to_csv('milestone1/desc.csv', encoding='utf-8')
print(desc)
print('\n\n')

# Print CV Error
fold_scores = common.calculate_cv_scores(classifier, X, y, folds=K_FOLDS)
print("--%d-Fold Cross-Validaton--" % K_FOLDS)
print("Mean:\t%.8f" % fold_scores.mean())
print("Var:\t%.8f\n" % fold_scores.var())

# Plot ROC Curve
curve = common.generate_roc_curve(classifier, X, y, folds=K_FOLDS)
print('--ROC Curve--')
print('AUC:\t%f\n' % curve.auc_mean)
plt.plot(curve.fp_domain, curve.tp_means)
plt.xlabel('False Positive Rate')
plt.ylabel('True Positive Rate')
plt.title('ROC Curve (%d-fold cross validation)' % K_FOLDS)
plt.savefig('milestone1/roc_curve.png')
plt.show()
