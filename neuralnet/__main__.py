import numpy as np
import matplotlib.pyplot as plt
import common
from sklearn.preprocessing import RobustScaler
from sklearn.neural_network import MLPClassifier

data=common.load_data()
X, y = common.split_xy(data)
X_scaled = RobustScaler().fit_transform(X)

K_FOLDS=10
ALPHA=3e-3
LAYERS = (16,16,8)
ACTIVATION = 'tanh'

# clf = MLPClassifier(
#     solver='lbfgs',
#     alpha=ALPHA,
#     hidden_layer_sizes=LAYERS,
#     activation=ACTIVATION)
#
# curve = common.generate_roc_curve(clf, X_scaled, y, folds=K_FOLDS)
# print('AUC:\t%f\n' % curve.auc_mean)

ALPHA_RANGE = [5e-4, 1e-3, 5e-3]

for alpha in ALPHA_RANGE:
    clf = MLPClassifier(
        solver='lbfgs',
        alpha=alpha,
        hidden_layer_sizes=LAYERS,
        activation=ACTIVATION)

    curve = common.generate_roc_curve(clf, X_scaled, y, folds=K_FOLDS)
    print("Alpha %f: %f" % (alpha, curve.auc_mean))

plt.xlabel("Alpha")
plt.ylabel("AUC")
AUCs=[]
for clf in classifiers:
        clf.fit(X_scaled, y)
        curve = common.generate_roc_curve(clf, X_scaled, y, folds=K_FOLDS)
        AUCs.append(curve.auc_mean)
plt.plot(alphas,AUCs)
plt.show()

#OneVsOneClassifier(LinearSVC(random_state=0)).fit(X, y).predict(X)
